from django.apps import AppConfig


class SendgridMessagesConfig(AppConfig):
    name = 'sendgrid_messages'
