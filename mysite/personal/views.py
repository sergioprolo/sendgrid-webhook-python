from django.shortcuts import render

homeDict = {}

contactDict = {'dictContent':['Contact information:','email : smauricioj@gmail.com','tel : 996-881-009']}

# Create your views here.

def home(request):
	return render(request, 'personal/home.html', homeDict)
	
def contact(request):
	return render(request, 'personal/contact.html', contactDict)