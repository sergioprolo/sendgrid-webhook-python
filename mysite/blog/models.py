from django.db import models

# Create your models here.
# any information needed to understand what's happening here, look up:
# https://docs.djangoproject.com/en/1.11/ref/models/fields/
# but, you can see any class that extends 'models.Model' as a table
# and the fields as the columns :D 

class Post(models.Model):
    title = models.CharField(max_length = 140)
    body = models.TextField()
    date = models.DateTimeField()

    def __str__(self):
        return self.title