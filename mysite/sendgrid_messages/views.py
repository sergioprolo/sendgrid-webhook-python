import copy, json, datetime
from django.utils import timezone
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from pandas import DataFrame, read_json
from os import path

actual_path = path.dirname(path.abspath(__file__))

@csrf_exempt
@require_POST
def webhook(request):
  jsondata = request.body
  data = json.loads(jsondata)
  meta = copy.copy(request.META)
  for k, v in meta.items():
   	if not isinstance(v, basestring):
   		del meta[k]
	dframe = read_json(jsondata)
  path_to_data = path.join(actual_path,
                           'data\\data-{}-{}.txt'.format(datetime.date.today(),
                                                         datetime.datetime.now().strftime("%H-%M-%S")))
  with open(path_to_data, 'w') as datafile:
		dframe.to_csv(datafile,
									encoding='utf-8')
  return HttpResponse(status=200)