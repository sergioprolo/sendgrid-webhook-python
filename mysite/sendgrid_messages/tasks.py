from celery.task import PeriodicTask
from celery.schedules import crontab

from .models import MessageSendGrid, WebhookTransaction


class ProcessMessages(PeriodicTask):

    run_every = crontab()  # this will run once a minute

    def run(self, **kwargs):
        unprocessed_trans = self.get_transactions_to_process()

        for trans in unprocessed_trans:
            try:
                self.process_trans(trans)
                trans.status = WebhookTransaction.PROCESSED
                trans.save()

            except Exception:
                trans.status = WebhookTransaction.ERROR
                trans.save()

    def get_transactions_to_process(self):
        return WebhookTransaction.objects.filter(
            event_name__in=self.event_names,
            status=WebhookTransaction.UNPROCESSED
        )
        
    def process_trans(self, trans):
        return MessageSendGrid.objects.create(
            ID=trans.body['ID'],
            event=trans.body['event'],
            email=trans.body['email'],
            timestamp=trans.body['timestamp'],
            sg_event_id=trans.body['sg_event_id'],
            sg_message_id=trans.body['sg_message_id'],
            marketing_campaign_id=trans.body['marketing_campaign_id'],
            marketing_campaign_name=trans.body['marketing_campaign_name'],
            marketing_campaign_version=trans.body['marketing_campaign_version'],
            ip=trans.body['ip'],
            webhook_transaction=trans
        )